package net.robiotic.weatherirl;

class WeatherState {
    /**
     * https://openweathermap.org/weather-conditions
     */
    private final int condition;

    /**
     * Conditions are 3-digit numbers, but
     * the first digit defines the group.
     */
    private final int conditionGroup;

    WeatherState(int condition) {
        this.condition = condition;
        this.conditionGroup = condition / 100;
    }

    boolean isRaining() {
        switch (conditionGroup) {
            case 2:
            case 3:
            case 5:
                return true;
            default:
                return false;
        }
    }

    boolean isThundering() {
        return conditionGroup == 2;
    }

    boolean isSnowing() {
        return conditionGroup == 6;
    }
}
