package net.robiotic.weatherirl;

import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

class WorldWeather extends BukkitRunnable {
    private static final String API = "https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s";

    private final WeatherIRL plugin;
    private final World world;
    private final double latitude, longitude;

    WorldWeather(WeatherIRL plugin, World world, double latitude, double longitude) {
        this.plugin = plugin;
        this.world = world;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void run() {
        final WeatherState state;

        // Get weather data from API
        try {
            state = fetchWeather();
        } catch (IOException exception) {
            plugin.getLogger().warning("Could not download weather data");
            return;
        } catch (InvalidConfigurationException exception) {
            plugin.getLogger().warning("Invalid weather data received");
            return;
        }

        // Apply weather to world until next refresh
        world.setStorm(state.isRaining());
        world.setThundering(state.isThundering());
        world.setWeatherDuration(plugin.interval);
    }

    @Override
    public synchronized void cancel() throws IllegalStateException {
        // Allow normal game weather patterns to resume
        world.setWeatherDuration(0);
        super.cancel();
    }

    /**
     * Download the latest weather data from OpenWeatherMap.
     *
     * @return A WeatherState object with the current weather code
     * @throws IOException                   If there is a network problem
     * @throws InvalidConfigurationException If the returned data is invalid
     */
    private WeatherState fetchWeather() throws IOException, InvalidConfigurationException {
        // Download
        final URL url = new URL(String.format(API, latitude, longitude, plugin.apiKey));
        final URLConnection request = url.openConnection();
        request.setConnectTimeout(500);
        request.setReadTimeout(500);
        request.connect();

        // Parse
        final YamlConfiguration yaml = new YamlConfiguration();
        yaml.load(new InputStreamReader((InputStream) request.getContent()));

        // Extract
        final int weatherCode = Integer.parseInt(yaml.getMapList("weather").get(0).get("id").toString());
        plugin.getLogger().fine(String.format(
                "[%s] Weather at %d is %d",
                world.getName(), yaml.getInt("id", 0), weatherCode
        ));
        return new WeatherState(weatherCode);
    }
}
